function [PAA, expPAA] = dm_paa(s, segLen)
// Piecewise Aggregate Approximation
//Calling Sequence
// PAA = dm_paa(s, win_size)
// [PAA, expPAA] = dm_paa(s, win_size)
// Parameters
// s: sequence vector (1xN)
// segLen: the number of data points on the raw time series that will be mapped to a single PAA coefficient
// PAA: PAA coefficients (1xN/segLen)
// expPAA: PAA sequence (1xN)

N = max(size(s)); // length of sequence
numCoeff=N/segLen;
if pmodulo(N,numCoeff)~=0 then
  segLen = floor(N/numCoeff)+1;
  s(N:segLen*numCoeff)=s($);
  N = max(size(s));
end;
sN = matrix(s, floor(segLen), numCoeff); // break in segments
PAA = mean(sN,'r'); // average segments

expPAA = repmat(PAA, segLen, 1); // expand segments
expPAA = expPAA(:)'; // make row
endfunction